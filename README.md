# MRC-gapps

Unified Google Apps repository for all supported Android versions.

# Supported versions
1. Android 6.0.1 Marshmallow
2. Android 7.0.0 Nougat
3. Android 7.1.0 Nougat
4. Android 8.0.0 Oreo
5. Android 8.1.0 Oreo

